#!/bin/bash
# SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
# SPDX-License-Identifier: MIT


# Exit when any command fails
set -e

# Check that the script is working after installation from the registry
astronaut-analysis data/astronauts.json
test -f boxplot.png
test -f combined_histogram.png
test -f female_humans_in_space.png
test -f humans_in_space.png
test -f male_humans_in_space.png
echo "Successfully created the plots"
