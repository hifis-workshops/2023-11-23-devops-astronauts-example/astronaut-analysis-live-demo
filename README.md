<!--
SPDX-FileCopyrightText: 2018 German Aerospace Center (DLR)
SPDX-License-Identifier: CC-BY-4.0
-->


# Astronaut Analysis

This analysis is based on publicly available astronauts data from [Wikidata](https://www.wikidata.org/wiki/Wikidata:Main_Page).
In this context, we investigated aspects such as time humans spent in space as well as the age distribution of the astronauts.

<img src="results/humans_in_space.png" alt="Total Time Human in Space" title="Total Time Human in Space" width="250" height="200" />
<img src="results/female_humans_in_space.png" alt="Total Time Females in Space" title="Total Time Females in Space" width="250" height="200" />
<img src="results/male_humans_in_space.png" alt="Total Time Males in Space" title="Total Time Males in Space" width="250" height="200" />
<img src="results/boxplot.png" alt="Age Distribution Box Plot" title="Age Distribution Box Plot" width="250" height="200" />
<img src="results/combined_histogram.png" alt="Age Distribution Histogram" title="Age Distribution Histogram" width="250" height="200" />

The repository is organized as follows:

- [data](data): Contains the astronauts data set retrieved from Wikidata
- [code](code): Contains the astronaut analysis script
- [results](results): Contains the resulting analysis plots

## Citation

If you use this work in a research publication, please cite the **specific version** that you used using the [following citation metadata](CITATION.cff).

## Astronaut Data

The data set has been generated using the following SPARQL query [[1]] (retrieval date: 2018-10-25).

You can also analyze a recent version of the astronaut data by replacing the data set and re-running the analysis script:
- Run the SPARQL query
- Download the resulting data formatted as JSON
- Replace the file `data/astronauts.json`
- Run the analysis script

[1]: https://query.wikidata.org/#%23Birthplaces%20of%20astronauts%0ASELECT%20DISTINCT%20%3Fastronaut%20%3FastronautLabel%20%3Fbirthdate%20%3FbirthplaceLabel%20%3Fsex_or_genderLabel%20%3Ftime_in_space%20%3Fdate_of_death%20WHERE%20%7B%0A%20%20%3Fastronaut%20%3Fx1%20wd%3AQ11631.%0A%20%20%3Fastronaut%20wdt%3AP569%20%3Fbirthdate.%0A%20%20%3Fastronaut%20wdt%3AP19%20%3Fbirthplace.%0A%20%20SERVICE%20wikibase%3Alabel%20%7B%20bd%3AserviceParam%20wikibase%3Alanguage%20%22en%22.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fastronaut%20wdt%3AP21%20%3Fsex_or_gender.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fastronaut%20wdt%3AP2873%20%3Ftime_in_space.%20%7D%0A%20%20OPTIONAL%20%7B%20%3Fastronaut%20wdt%3AP570%20%3Fdate_of_death.%20%7D%0A%7D%0AORDER%20BY%20DESC%28%3Ftime_in_space%29

## Astronaut Analysis Script

The script requires Python >= 3.8 and uses the libraries [pandas](https://pandas.pydata.org/) (*BSD 3-Clause License*) as well as [matplotlib](https://matplotlib.org/) (*Matplotlib License*).

> The script has been successfully tested on Windows 10 and Linux with Python 3.8.

### Installation

Please clone this repository and install the [required dependencies](code/requirements.txt) as follows:

```bash
git clone ...
cd <CLONED_DIRECTORY>/code
pip install -r requirements.txt
```

> Please note that the `requirements.txt` pins the exact version which have been used to create the [reference results](results).

### Usage

You can run the script as follows:

```bash
python astronaut_analysis.py <PATH_TO_THE_ASTRONAUTS_DATA_FILE>
```

The script processes the specified astronauts data set file (e.g., the [astronauts reference data set](data/astronauts.json) and stores the plots in the directory in which you run the script.
Existing result plots will be overwritten without notice.

### Testing

The [test.sh](code/test.sh) script performs some basic checks to support maintaining the analysis script:

- It installs the required packages.
- It runs the [flake8](https://flake8.pycqa.org/en/latest/) linter to find programming mistakes and code style issues.
- It runs the [reuse tool](https://git.fsfe.org/reuse/tool) to ensure that all relevant copyright and licensing information are specified.
- It runs the analysis script and checks that the expected plots are produced.

> The script runs as part of the [GitLab build pipeline](.gitlab-ci.yml) to find errors introduced by new commits.

### Packaging

We provide installable Python packages of the analysis script via the [PyPi registry of the GitLab project](https://codebase.helmholtz.cloud/hifis-workshops/2023-10-20-workshop-on-research-software-development-for-ias-8/part-1-good-practices-in-research-software-development-and-publication/05-astronaut-analysis-release-a-citable-software-version/-/packages) for convenience.
 
#### Python Package Creation and Publication

The Python packages can be created manually as follows:

```bash
pip install -r code/requires-packaging.txt
python -m build
```

The resulting Python packages are created using `setuptools` and are stored in the `dist` directory.
The Python source package contains the analysis script, the data set, the reference results as well as the documentation.
For further information, please see the [Python Packaging Tutorial](https://packaging.python.org/en/latest/tutorials/packaging-projects/) and the [Setuptools Quickstart Tutorial](https://setuptools.pypa.io/en/latest/userguide/quickstart.html).

On this basis, the created Python packages can be manually published to the PyPi registry of the GitLab project using `twine` as follows:

```
python -m twine upload --repository-url https://codebase.helmholtz.cloud/api/v4/projects/9793/packages/pypi/simple dist/*
```

For further details such as the available authentication options, please see the [GitLab documentation concerning the usage of the integrated PyPi registry](https://codebase.helmholtz.cloud/help/user/packages/pypi_repository/index#pypi-packages-in-the-package-registry).

> Creation and publication of the Python packages are performed automatically via the [GitLab build pipeline](.gitlab-ci.yml) when a new release tag is created.

#### Install a published Python Package

You can install a published Python package from the PyPi registry of the GitLab project via `pip` as follows:

```bash
pip install astronaut-analysis --index-url https://codebase.helmholtz.cloud/api/v4/projects/9793/packages/pypi/simple
```

**Please note:**
- You require Python >= 3.8.
  The required run-time dependencies are installed automatically.
- You can run the script via `astronauts-analysis <PATH_TO_THE_ASTRONAUTS_DATA_FILE>` after the installation.
- The GitLab project and its registry are public.
  For that reason you do not require any credentials when accessing the registry.
  For further information, please see the [PyPi registry of the GitLab project](https://codebase.helmholtz.cloud/hifis-workshops/2023-10-20-workshop-on-research-software-development-for-ias-8/part-1-good-practices-in-research-software-development-and-publication/05-astronaut-analysis-release-a-citable-software-version/-/packages). 

## Changes

The [changelog](CHANGELOG.md) documents all notable changes.

## License

Please see the file [LICENSE.md](LICENSE.md) for further information about how the content is licensed.
